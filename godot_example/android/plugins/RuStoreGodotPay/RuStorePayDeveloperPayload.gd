# RuStorePayDeveloperPayload
# @brief
#	Указанная разработчиком строка,
#	содержащая дополнительную информацию о заказе.
class_name RuStorePayDeveloperPayload extends RuStorePayBaseValue

func _init(val: String):
	super(val)
