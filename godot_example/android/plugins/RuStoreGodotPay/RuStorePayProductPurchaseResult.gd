# RuStorePayProductPurchaseResult
# @brief Родительский класс результатов покупки.
class_name RuStorePayProductPurchaseResult extends Object

# @brief Результат успешного завершения покупки цифрового товара.
class SuccessProductPurchaseResult extends RuStorePayProductPurchaseResult:

	# @brief Идентификатор счёта.
	var invoiceId: RuStorePayInvoiceId = null
	
	# @brief
	#	Уникальный идентификатор оплаты, сформированный приложением (необязательный параметр).
	#	Если вы укажете этот параметр в вашей системе, вы получите его в ответе при работе с API.
	#	Если не укажете, он будет сгенерирован автоматически (uuid).
	#	Максимальная длина 150 символов.
	var orderId: RuStorePayOrderId = null
	
	# @brief Идентификатор продукта, который был присвоен продукту в консоли RuStore.
	var productId: RuStorePayProductId = null
	
	# @brief Идентификатор покупки.
	var purchaseId: RuStorePayPurchaseId = null
	
	# @brief Токен для валидации покупки на сервере (необязательный параметр).
	var subscriptionToken: RuStorePaySubscriptionToken = null


# @brief
#	Запрос на покупку отправлен,
#	при этом пользователь закрыл «платёжную шторку» на своём устройстве, и результат оплаты неизвестен.
class CancelProductPurchaseResult extends RuStorePayProductPurchaseResult:

	# @brief Идентификатор покупки.
	var purchaseId: RuStorePayPurchaseId = null


# @brief
#	При отправке запроса на оплату или получения статуса оплаты возникла проблема,
#	невозможно установить статус покупки.
class FailureProductPurchaseResult extends RuStorePayProductPurchaseResult:

	# @brief Информация об ошибке.
	var cause: RuStoreError = null
	
	# @brief Идентификатор счёта (необязательный параметр).
	var invoiceId: RuStorePayInvoiceId = null
	
	# @brief
	#	Уникальный идентификатор оплаты, сформированный приложением (необязательный параметр).
	#	Если вы укажете этот параметр в вашей системе, вы получите его в ответе при работе с API.
	#	Если не укажете, он будет сгенерирован автоматически (uuid).
	var orderId: RuStorePayOrderId = null
	
	# @brief Идентификатор продукта, который был присвоен продукту в консоли RuStore (необязательный параметр).
	var productId: RuStorePayProductId = null
	
	# @brief Идентификатор покупки (необязательный параметр).
	var purchaseId: RuStorePayPurchaseId = null
	
	# @brief Количество продукта (необязательный параметр).
	var quantity: RuStorePayQuantity = null
	
	# @brief Токен для валидации покупки на сервере (необязательный параметр).
	var subscriptionToken: RuStorePaySubscriptionToken = null
