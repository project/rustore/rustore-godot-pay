# RuStorePayPurchaseId
# @brief Идентификатор покупки.
class_name RuStorePayPurchaseId extends RuStorePayBaseValue

func _init(val: String):
	super(val)
