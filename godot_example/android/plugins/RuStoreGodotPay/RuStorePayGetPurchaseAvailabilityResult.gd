# RuStorePayGetPurchaseAvailabilityResult
# @brief Проверка доступности функционала.
class_name RuStorePayGetPurchaseAvailabilityResult extends Object

var isAvailable: bool = false
var cause: RuStoreError = null
