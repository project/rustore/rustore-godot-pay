# RuStorePayAmountLabel
# @brief Отформатированная цена товара, включая валютный знак.
class_name RuStorePayAmountLabel extends RuStorePayBaseValue

func _init(val: String):
	super(val)
