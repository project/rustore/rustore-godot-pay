# RuStorePayOrderId
# @brief
#	Уникальный идентификатор оплаты,
#	указанный разработчиком или сформированный автоматически (uuid).
class_name RuStorePayOrderId extends RuStorePayBaseValue

func _init(val: String):
	super(val)
