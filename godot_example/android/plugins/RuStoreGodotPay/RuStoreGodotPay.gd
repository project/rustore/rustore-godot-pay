# RuStoreGodotPayClient
# @brief Класс реализует API для интегрирации платежей в мобильное приложение.
class_name RuStoreGodotPayClient extends Object

const SINGLETON_NAME = "RuStoreGodotPay"

var _isInitialized: bool = false
var _clientWrapper: Object = null

var _core_client: RuStoreGodotCoreUtils = null

# @brief Действие, выполняемое при успешном завершении операции get_purchase_availability.
signal on_get_purchase_availability_success

# @brief Действие, выполняемое в случае ошибки get_purchase_availability.
signal on_get_purchase_availability_failure

# @brief Действие, выполняемое при успешном завершении операции get_products.
signal on_get_products_success

# @brief Действие, выполняемое в случае ошибки get_products.
signal on_get_products_failure

# @brief Действие, выполняемое при успешном завершении операции get_purchases.
signal on_get_purchases_success

# @brief Действие, выполняемое в случае ошибки get_purchases.
signal on_get_purchases_failure

# @brief Действие, выполняемое при успешном завершении операции get_purchase.
signal on_get_purchase_success

# @brief Действие, выполняемое в случае ошибки get_purchase.
signal on_get_purchase_failure

# @brief Действие, выполняемое при успешном завершении операции purchase_one_step.
signal on_purchase_one_step_success

# @brief Действие, выполняемое в случае ошибки purchase_one_step.
signal on_purchase_one_step_failure

# @brief Действие, выполняемое при успешном завершении операции purchase_two_step.
signal on_purchase_two_step_success

# @brief Действие, выполняемое в случае ошибки purchase_two_step.
signal on_purchase_two_step_failure

# @brief Действие, выполняемое при успешном завершении операции confirm_two_step_purchase.
signal on_confirm_two_step_purchase_success

# @brief Действие, выполняемое в случае ошибки confirm_two_step_purchase.
signal on_confirm_two_step_purchase_failure

# @brief Действие, выполняемое при успешном завершении операции cancel_two_step_purchas.
signal on_cancel_two_step_purchase_success

# @brief Действие, выполняемое в случае ошибки cancel_two_step_purchas.
signal on_cancel_two_step_purchase_failure

static var _instance: RuStoreGodotPayClient = null


# @brief
#	Получить экземпляр RuStoreGodotPayClient.
# @return
#	Возвращает указатель на единственный экземпляр RuStoreGodotPayClient (реализация паттерна Singleton).
#	Если экземпляр еще не создан, создает его.
static func get_instance() -> RuStoreGodotPayClient:
	if _instance == null:
		_instance = RuStoreGodotPayClient.new()
	return _instance


func _init():
	_core_client = RuStoreGodotCoreUtils.get_instance()
	_clientWrapper = Engine.get_singleton(SINGLETON_NAME)
	_clientWrapper.rustore_get_purchase_availability_success.connect(_on_get_purchase_availability_success)
	_clientWrapper.rustore_get_purchase_availability_failure.connect(_on_get_purchase_availability_failure)
	_clientWrapper.rustore_on_get_products_success.connect(_on_get_products_success)
	_clientWrapper.rustore_on_get_products_failure.connect(_on_get_products_failure)
	_clientWrapper.rustore_on_get_purchases_success.connect(_on_get_purchases_success)
	_clientWrapper.rustore_on_get_purchases_failure.connect(_on_get_purchases_failure)	
	_clientWrapper.rustore_on_get_purchase_success.connect(_on_get_purchase_success)
	_clientWrapper.rustore_on_get_purchase_failure.connect(_on_get_purchase_failure)
	_clientWrapper.rustore_on_purchase_one_step_success.connect(_on_purchase_one_step_success)
	_clientWrapper.rustore_on_purchase_one_step_failure.connect(_on_purchase_one_step_failure)
	_clientWrapper.rustore_on_purchase_two_step_success.connect(_on_purchase_two_step_success)
	_clientWrapper.rustore_on_purchase_two_step_failure.connect(_on_purchase_two_step_failure)
	_clientWrapper.rustore_on_confirm_two_step_purchase_success.connect(_on_confirm_two_step_purchase_success)
	_clientWrapper.rustore_on_confirm_two_step_purchase_failure.connect(_on_confirm_two_step_purchase_failure)
	_clientWrapper.rustore_on_cancel_two_step_purchase_success.connect(_on_cancel_two_step_purchase_success)
	_clientWrapper.rustore_on_cancel_two_step_purchase_failure.connect(_on_cancel_two_step_purchase_failure)


# Get purchase availability
# @brief Проверка доступности платежей.
func get_purchase_availability():
	_clientWrapper.getPurchaseAvailability()

func _on_get_purchase_availability_success(data: String):
	var obj = RuStorePayJsonParser.ToPurchaseAvailabilityResult(data)
	on_get_purchase_availability_success.emit(obj)

func _on_get_purchase_availability_failure(data: String):
	var obj = RuStoreError.new(data)
	on_get_purchase_availability_failure.emit(obj)


# Is RuStore installed
# @brief Проверка установлен ли на устройстве пользователя RuStore.
# @return Возвращает true, если RuStore установлен, в противном случае — false.
func is_rustore_installed() -> bool:
	return _clientWrapper.isRuStoreInstalled()


# Get products
# @brief Получение списка продуктов, добавленных в ваше приложение через консоль RuStore.
# @param productIds
#	Список идентификаторов продуктов (задаются при создании продукта в консоли разработчика).
#	Список продуктов имеет ограничение в размере 1000 элементов.
func get_products(productIds: Array[RuStorePayProductId]):
	var ids: Array[String] = []
	for item in productIds:
		ids.push_back(item.value)
	_clientWrapper.getProducts(ids)

func _on_get_products_success(data: String):
	var obj_arr: Array[RuStorePayProduct] = []
	var str_arr = JSON.parse_string(data)
	for str_item in str_arr:
		var obj_item: RuStorePayProduct = RuStorePayJsonParser.ToPayProduct(str(str_item))
		obj_arr.append(obj_item)
	on_get_products_success.emit(obj_arr)

func _on_get_products_failure(data: String):
	var obj = RuStoreError.new(data)
	on_get_products_failure.emit(obj)


# Get purchases
# @brief Получение списка покупок пользователя.
func get_purchases():
	_clientWrapper.getPurchases()

func _on_get_purchases_success(data: String):
	var obj_arr: Array[RuStorePayPurchase] = []
	var str_arr = JSON.parse_string(data)
	for str_item in str_arr:
		var obj_item = RuStorePayJsonParser.ToPayPurchase(str(str_item))
		obj_arr.append(obj_item)
	on_get_purchases_success.emit(obj_arr)

func _on_get_purchases_failure(data: String):
	var obj = RuStoreError.new(data)
	on_get_purchases_failure.emit(obj)


# Get purchase
# @brief Получение информации о покупке.
# @param purchaseId Идентификатор продукта, который был присвоен продукту в консоли RuStore.
func get_purchase(purchase_id: RuStorePayPurchaseId):
	_clientWrapper.getPurchase(purchase_id.value)

func _on_get_purchase_success(data: String):
	var obj = RuStorePayJsonParser.ToPayPurchase(data)
	on_get_purchase_success.emit(obj)

func _on_get_purchase_failure(purchase_id: String, data: String):
	var id = RuStorePayPurchaseId.new(purchase_id)
	var obj = RuStoreError.new(data)
	on_get_purchase_failure.emit(purchase_id, obj)


# Purchase
# @brief Покупки продукта с одностадийной оплатой.
# @param parameters Параметры покупки продукт
func purchase_one_step(parameters: RuStorePayProductPurchaseParams):
	var params: Dictionary = { "productId" : parameters.productId.value }
	if parameters.developerPayload != null:
		params["developerPayload"] = parameters.developerPayload.value
	if parameters.orderId != null:
		params["orderId"] = parameters.orderId.value
	if parameters.quantity != null:
		params["quantity"] = parameters.quantity.value
	_clientWrapper.purchaseOneStep(params)

func _on_purchase_one_step_success(data: String):
	var obj = RuStorePayJsonParser.ToRuStorePayProductPurchaseResult(data)
	on_purchase_one_step_success.emit(obj)

func _on_purchase_one_step_failure(productId: String, data: String):
	var id = RuStorePayProductId.new(productId)
	var obj = RuStoreError.new(data)
	on_purchase_one_step_failure.emit(id, obj)


# @brief Покупки продукта с двустадийной оплатой.
# @param parameters Параметры покупки продукта.
func purchase_two_step(parameters: RuStorePayProductPurchaseParams):
	var params: Dictionary = { "productId" : parameters.productId.value }
	if parameters.developerPayload != null:
		params["developerPayload"] = parameters.developerPayload.value
	if parameters.orderId != null:
		params["orderId"] = parameters.orderId.value
	if parameters.quantity != null:
		params["quantity"] = parameters.quantity.value
	_clientWrapper.purchaseTwoStep(params)

func _on_purchase_two_step_success(data: String):
	var obj = RuStorePayJsonParser.ToRuStorePayProductPurchaseResult(data)
	on_purchase_two_step_success.emit(obj)

func _on_purchase_two_step_failure(productId: String, data: String):
	var id = RuStorePayProductId.new(productId)
	var obj = RuStoreError.new(data)
	on_purchase_two_step_failure.emit(id, obj)


# Confirm purchase
# @brief
#	Потребление (подтверждение) покупки.
#	После вызова подтверждения покупка перейдёт в статус CONFIRMED.
#	Запрос на потребление (подтверждение) покупки должен сопровождаться выдачей товара.
# @param purchase_id Идентификатор покупки.
# @param developer_payload Строка, содержащая дополнительную информацию о заказе (необязательный параметр).
func confirm_two_step_purchase(purchase_id: RuStorePayPurchaseId, developer_payload: RuStorePayDeveloperPayload = null):
	_clientWrapper.confirmTwoStepPurchase(purchase_id.value, developer_payload.value)

func _on_confirm_two_step_purchase_success(purchase_id: String):
	var id = RuStorePayPurchaseId.new(purchase_id)
	on_confirm_two_step_purchase_success.emit(id)

func _on_confirm_two_step_purchase_failure(purchase_id: String, data: String):
	var id = RuStorePayPurchaseId.new(purchase_id)
	var obj = RuStoreError.new(data)
	on_confirm_two_step_purchase_failure.emit(id, obj)


# Cancel purchase
# @brief
#	Отмена покупки.
#	Запрос на потребление (подтверждение) покупки должен сопровождаться выдачей товара.
# @param purchase_id Идентификатор покупки.
func cancel_two_step_purchase(purchase_id: RuStorePayPurchaseId):
	_clientWrapper.cancelTwoStepPurchase(purchase_id.value)

func _on_cancel_two_step_purchase_success(purchase_id: String):
	var id = RuStorePayPurchaseId.new(purchase_id)
	on_cancel_two_step_purchase_success.emit(id)

func _on_cancel_two_step_purchase_failure(purchase_id: String, data: String):
	var id = RuStorePayPurchaseId.new(purchase_id)
	var obj = RuStoreError.new(data)
	on_cancel_two_step_purchase_failure.emit(id, obj)
