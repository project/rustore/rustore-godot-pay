# ERuStorePayProductType
# @brief Тип продукта.
class_name ERuStorePayProductType extends Object

# @brief Доступные значения.
enum Item {
	APPLICATION,
	
	# @brief
	#	Непотребляемй товар.
	#	Можно купить один раз.
	NON_CONSUMABLE_PRODUCT,
	
	# @brief
	#	Потребляемый товар.
	#	Можно купить много раз.
	CONSUMABLE_PRODUCT
}
