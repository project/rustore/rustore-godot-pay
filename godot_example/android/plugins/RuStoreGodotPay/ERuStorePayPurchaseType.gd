# ERuStorePayPurchaseType
# @brief Тип покупки.
class_name ERuStorePayPurchaseType extends Object

# @brief Доступные значения.
enum Item {
	# @brief Одностадийная оплата.
	ONE_STEP,
	
	# @brief Двухстадийная оплата.
	TWO_STEP
}
