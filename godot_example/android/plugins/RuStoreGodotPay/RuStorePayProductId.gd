# RuStorePayProductId
# @brief Идентификатор продукта, указанный при создании продукта в консоли разработчика.
class_name RuStorePayProductId extends RuStorePayBaseValue

func _init(val: String):
	super(val)
