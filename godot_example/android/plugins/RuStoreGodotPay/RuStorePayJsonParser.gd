class_name RuStorePayJsonParser extends Object

static func ToPurchaseAvailabilityResult(json: String = "") -> RuStorePayGetPurchaseAvailabilityResult:
	var result: RuStorePayGetPurchaseAvailabilityResult = null
	if json != "":
		var obj = JSON.parse_string(json)
		result.isAvailable = obj["isAvailable"]
		
		if obj.has("cause"):
			var jcause = JSON.stringify(obj["cause"])
			result.cause = RuStoreError.new(jcause)
	
	return result

static func ToPayProduct(json: String = "") -> RuStorePayProduct:
	var product: RuStorePayProduct = null
	if json != "":
		var obj = JSON.parse_string(json)
		product = RuStorePayProduct.new()
		
		product.amountLabel = RuStorePayAmountLabel.new(obj["amountLabel"]["value"])
		product.currency = RuStorePayCurrency.new(obj["currency"]["value"])
		if obj.has("description"):
			product.description = RuStorePayDescription.new(obj["description"]["value"])
		product.imageUrl = RuStorePayUrl.new(obj["imageUrl"]["value"])
		if obj.has("price"):
			product.price = RuStorePayPrice.new(obj["price"]["value"])
		product.productId = RuStorePayProductId.new(obj["productId"]["value"])
		if obj.has("promoImageUrl"):
			product.promoImageUrl = RuStorePayUrl.new(obj["promoImageUrl"]["value"])
		product.title = RuStorePayTitle.new(obj["title"]["value"])
		product.type = ERuStorePayProductType.Item.get(obj["type"])
	
	return product

static func ToPayPurchase(json: String = "") -> RuStorePayPurchase:
	var purchase: RuStorePayPurchase = null
	if json != "":
		var obj = JSON.parse_string(json)
		purchase = RuStorePayPurchase.new()
		
		purchase.amountLabel = RuStorePayAmountLabel.new(obj["amountLabel"]["value"])
		purchase.currency = RuStorePayCurrency.new(obj["currency"]["value"])
		purchase.description = RuStorePayDescription.new(obj["description"]["value"])
		if obj.has("developerPayload"):
			purchase.developerPayload = RuStorePayDeveloperPayload.new(obj["developerPayload"]["value"])
		purchase.invoiceId = RuStorePayInvoiceId.new(obj["invoiceId"]["value"])
		if obj.has("orderId"):
			purchase.orderId = RuStorePayOrderId.new(obj["orderId"]["value"])
		purchase.price = RuStorePayPrice.new(obj["price"]["value"])
		purchase.productId = RuStorePayProductId.new(obj["productId"]["value"])
		purchase.productType = ERuStorePayProductType.Item.get(obj["productType"])
		purchase.purchaseId = RuStorePayPurchaseId.new(obj["purchaseId"]["value"])
		if obj.has("purchaseTime"):
			purchase.purchaseTime = RuStorePayTime.new(obj["purchaseTime"])
		purchase.purchaseType = ERuStorePayPurchaseType.Item.get(obj["purchaseType"])
		purchase.quantity = RuStorePayQuantity.new(obj["quantity"]["value"])
		purchase.status = ERuStorePayPurchaseStatus.Item.get(obj["status"])
		if obj.has("subscriptionToken"):
			purchase.subscriptionToken = RuStorePaySubscriptionToken.new(obj["subscriptionToken"]["value"])
	
	return purchase

static func ToRuStorePayProductPurchaseResult(json: String = "") -> RuStorePayProductPurchaseResult:
	if json != "":
		var obj = JSON.parse_string(json)
		var type = obj["type"]
		var data = str(obj["data"])
		if type == "SuccessProductPurchaseResult":
			var success = RuStorePayProductPurchaseResult.SuccessProductPurchaseResult.new()
			success.invoiceId = RuStorePayInvoiceId.new(obj["invoiceId"]["value"])
			if obj.has("orderId"):
				success.orderId = RuStorePayOrderId.new(obj["orderId"]["value"])
			success.productId = RuStorePayProductId.new(obj["productId"]["value"])
			success.purchaseId = RuStorePayPurchaseId.new(obj["purchaseId"]["value"])
			if obj.has("subscriptionToken"):
				success.subscriptionToken = RuStorePaySubscriptionToken.new(obj["subscriptionToken"]["value"])
			
			return success
		elif type == "CancelProductPurchaseResult":
			var cancel = RuStorePayProductPurchaseResult.CancelProductPurchaseResult.new()
			if obj.has("purchaseId"):
				cancel.purchaseId = RuStorePayPurchaseId.new(obj["purchaseId"]["value"])
			
			return cancel
		elif type == "FailureProductPurchaseResult":
			var failure = RuStorePayProductPurchaseResult.FailureProductPurchaseResult.new()
			failure.cause = RuStoreError.new(JSON.stringify(obj["cause"]))
			if obj.has("invoiceId"):
				failure.invoiceId = RuStorePayInvoiceId.new(obj["invoiceId"]["value"]);
			if obj.has("orderId"):
				failure.orderId = RuStorePayOrderId.new(obj["orderId"]["value"]);
			if obj.has("productId"):
				failure.productId = RuStorePayProductId.new(obj["productId"]["value"]);
			if obj.has("purchaseId"):
				failure.purchaseId = RuStorePayPurchaseId.new(obj["purchaseId"]["value"]);
			if obj.has("quantity"):
				failure.quantity = RuStorePayQuantity.new(obj["quantity"]["value"]);
			if obj.has("subscriptionToken"):
				failure.subscriptionToken = RuStorePaySubscriptionToken.new(obj["subscriptionToken"]["value"]);
			
			return failure
		
	return RuStorePayProductPurchaseResult.new()
