# RuStorePaySubscriptionToken
# @brief Токен для валидации покупки на сервере.
class_name RuStorePaySubscriptionToken extends RuStorePayBaseValue

func _init(val: String):
	super(val)
