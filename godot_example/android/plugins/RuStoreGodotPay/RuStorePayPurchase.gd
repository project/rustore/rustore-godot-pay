# RuStorePayPurchase
# @brief Информация о покупке.
class_name RuStorePayPurchase extends Object

# @brief Отформатированная цена покупки, включая валютный знак.
var amountLabel: RuStorePayAmountLabel = null

# @brief Код валюты ISO 4217.
var currency: RuStorePayCurrency = null

# @brief Описание на языке language (необязательный параметр).
var description: RuStorePayDescription = null

# @brief
#	Строка с дополнительной информацией о заказе,
#	которую вы можете установить при инициализации процесса покупки (необязательный параметр).
var developerPayload: RuStorePayDeveloperPayload = null

# @brief Идентификатор счёта.
var invoiceId: RuStorePayInvoiceId = null

# @brief
#	Уникальный идентификатор оплаты, сформированный приложением (необязательный параметр).
#	Если вы укажете этот параметр в вашей системе, вы получите его в ответе при работе с API.
#	Если не укажете, он будет сгенерирован автоматически (uuid).
#	Максимальная длина 150 символов.
var orderId: RuStorePayOrderId = null

# @brief Цена в минимальных единицах (например в копейках).
var price: RuStorePayPrice = null

# @brief Идентификатор продукта, который был присвоен продукту в консоли RuStore.
var productId: RuStorePayProductId = null

# @brief Тип продукта.
var productType: ERuStorePayProductType.Item = 0

# @brief Идентификатор покупки.
var purchaseId: RuStorePayPurchaseId = null

# @brief Время покупки (необязательный параметр).
var purchaseTime: RuStorePayTime = null

# @brief Тип покупки.
var purchaseType: ERuStorePayPurchaseType.Item = 0

# @brief Количество продукта.
var quantity: RuStorePayQuantity = null

# @brief Статус покупки.
var status: ERuStorePayPurchaseStatus.Item = 0

# @brief Токен для валидации покупки на сервере (необязательный параметр).
var subscriptionToken: RuStorePaySubscriptionToken = null
