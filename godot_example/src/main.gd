extends Node2D

@onready var _products_list = $CanvasLayer/VBoxContainer/TabContainer/Products/Products/ProductsList
@onready var _purchases_list = $CanvasLayer/VBoxContainer/TabContainer/Purchases/Purchases/PurchasesList
@onready var _loading = $CanvasLayer/LoadingPanel
@onready var _is_rustore_installed_label = $CanvasLayer/VBoxContainer/IsRuStoreInstalled/Label
@onready var _purchase_method = $CanvasLayer/purchase_method

var PRODUCT_IDS: Array[RuStorePayProductId] = [
	RuStorePayProductId.new("unity_non_con_1"),
	RuStorePayProductId.new("unity_con_2"),
]

var _core_client: RuStoreGodotCoreUtils = null
var _pay_client: RuStoreGodotPayClient = null

func _ready():
	_core_client = RuStoreGodotCoreUtils.get_instance()
	
	_pay_client = RuStoreGodotPayClient.get_instance()
	_pay_client.on_get_purchase_availability_success.connect(_on_get_purchase_availability_success)
	_pay_client.on_get_purchase_availability_failure.connect(_on_get_purchase_availability_failure)
	_pay_client.on_get_products_success.connect(_on_get_products_success)
	_pay_client.on_get_products_failure.connect(_on_get_products_failure)
	_pay_client.on_get_purchases_success.connect(_on_get_purchases_success)
	_pay_client.on_get_purchases_failure.connect(_on_get_purchases_failure)
	_pay_client.on_get_purchase_success.connect(_on_get_purchase_success)
	_pay_client.on_get_purchase_failure.connect(_on_get_purchase_failure)
	_pay_client.on_purchase_one_step_success.connect(_on_purchase_one_step_success)
	_pay_client.on_purchase_one_step_failure.connect(_on_purchase_one_step_failure)
	_pay_client.on_purchase_two_step_success.connect(_on_purchase_two_step_success)
	_pay_client.on_purchase_two_step_failure.connect(_on_purchase_two_step_failure)
	_pay_client.on_confirm_two_step_purchase_success.connect(_on_confirm_two_step_purchase_success)
	_pay_client.on_confirm_two_step_purchase_failure.connect(_on_confirm_two_step_purchase_failure)
	_pay_client.on_cancel_two_step_purchase_success.connect(_on_cancel_two_step_purchase_success)
	_pay_client.on_cancel_two_step_purchase_failure.connect(_on_cancel_two_step_purchase_failure)
	
	var is_rustore_installed: bool = _pay_client.is_rustore_installed()
	
	if(is_rustore_installed):
		_is_rustore_installed_label.text = "RuStore is installed [v]"
	else:
		_is_rustore_installed_label.text = "RuStore is not installed [x]"


func _on_tab_container_tab_clicked(tab):
	match tab:
		0:
			_on_update_products_list_button_pressed()
		1:
			_on_update_purchases_list_button_pressed()


# Get purchase availability
func _on_get_purchase_availability_button_pressed():
	_loading.visible = true
	_pay_client.get_purchase_availability()

func _on_get_purchase_availability_success(result: RuStorePayGetPurchaseAvailabilityResult):
	_loading.visible = false
	if result.isAvailable:
		_core_client.show_toast("Purchases are available")

func _on_get_purchase_availability_failure(error: RuStoreError):
	_loading.visible = false
	OS.alert(error.description, error.name)


# Update products list
func _on_update_products_list_button_pressed():
	_loading.visible = true
	_pay_client.get_products(PRODUCT_IDS)

func _on_get_products_success(products: Array[RuStorePayProduct]):
	_loading.visible = false
	for product_panel in _products_list.get_children():
		product_panel.queue_free()
	
	for product in products:
		var product_panel: ProductPanel = load("res://scenes/product.tscn").instantiate()
		_products_list.add_child(product_panel)
		product_panel.set_product(product)
		product_panel.on_purchase_pressed.connect(_on_purchase_pressed)

func _on_get_products_failure(error: RuStoreError):
	_loading.visible = false
	OS.alert(error.description, error.name)


# Get purchases
func _on_update_purchases_list_button_pressed():
	_loading.visible = true
	_pay_client.get_purchases()

func _on_get_purchases_success(purchases: Array[RuStorePayPurchase]):
	_loading.visible = false
	for purchase_panel in _purchases_list.get_children():
		purchase_panel.queue_free()

	for purchase in purchases:
		var purchase_panel: PurchasePanel = load("res://scenes/purchase.tscn").instantiate()
		_purchases_list.add_child(purchase_panel)
		_purchases_list.move_child(purchase_panel, 0)
		purchase_panel.set_purchase(purchase)
		purchase_panel.on_confirm_pressed.connect(_on_confirm_two_step_purchase_pressed)
		purchase_panel.on_cancel_pressed.connect(_on_cancel_two_step_purchase_pressed)
		purchase_panel.on_get_purchase_pressed.connect(_on_get_purchase_pressed)

func _on_get_purchases_failure(error: RuStoreError):
	_loading.visible = false
	OS.alert(error.description, error.name)


# Purchase
func _on_purchase_pressed(product: RuStorePayProduct):
	var parameters = RuStorePayProductPurchaseParams.new(product.productId)
	var one_step = func():
		_pay_client.purchase_one_step(parameters)
	var two_step = func():
		_pay_client.purchase_two_step(parameters)
	
	_purchase_method.show(
		product.title.value,
		one_step,
		two_step
	)

func _on_purchase_one_step_success(result: RuStorePayProductPurchaseResult):
	_on_purchase_success(result)

func _on_purchase_one_step_failure(product_id: RuStorePayProductId, error: RuStoreError):
	_on_purchase_failure(product_id, error)

func _on_purchase_two_step_success(result: RuStorePayProductPurchaseResult):
	_on_purchase_success(result)

func _on_purchase_two_step_failure(product_id: RuStorePayProductId, error: RuStoreError):
	_on_purchase_failure(product_id, error)

func _on_purchase_success(result: RuStorePayProductPurchaseResult):
	if is_instance_of(result, RuStorePayProductPurchaseResult.SuccessProductPurchaseResult):
		_core_client.show_toast("SuccessProductPurchaseResult")
	elif is_instance_of(result, RuStorePayProductPurchaseResult.CancelProductPurchaseResult):
		_core_client.show_toast("CancelProductPurchaseResult")
	elif is_instance_of(result, RuStorePayProductPurchaseResult.FailureProductPurchaseResult):
		_core_client.show_toast("FailureProductPurchaseResult")
	else:
		_core_client.show_toast("RuStorePayProductPurchaseResult")

func _on_purchase_failure(product_id: RuStorePayProductId, error: RuStoreError):
	OS.alert(error.description, error.name)


# Get purchase
func _on_get_purchase_pressed(purchase: RuStorePayPurchase):
	_loading.visible = true
	_pay_client.get_purchase(purchase.purchaseId)

func _on_get_purchase_success(purchase: RuStorePayPurchase):
	_loading.visible = false
	OS.alert(purchase.description.value, purchase.purchaseId.value)

func _on_get_purchase_failure(purchase_id: RuStorePayPurchaseId, error: RuStoreError):
	_loading.visible = false
	OS.alert(error.description, error.name)


# Confirm purchase
func _on_confirm_two_step_purchase_pressed(purchase: RuStorePayPurchase):
	_loading.visible = true
	_pay_client.confirm_two_step_purchase(purchase.purchaseId, purchase.developerPayload)

func _on_confirm_two_step_purchase_success(purchase_id: RuStorePayPurchaseId):
	_loading.visible = false
	_pay_client.get_purchases()
	_core_client.show_toast("Consume " + purchase_id.value)

func _on_confirm_two_step_purchase_failure(purchase_id: RuStorePayPurchaseId, error: RuStoreError):
	_loading.visible = false
	OS.alert(error.description, error.name)


# Cancel purchase
func _on_cancel_two_step_purchase_pressed(purchase: RuStorePayPurchase):
	_loading.visible = true
	_pay_client.cancel_two_step_purchase(purchase.purchaseId)

func _on_cancel_two_step_purchase_success(purchase_id: RuStorePayPurchaseId):
	_loading.visible = false
	_pay_client.get_purchases()
	_core_client.show_toast("Confirm " + purchase_id.value)

func _on_cancel_two_step_purchase_failure(purchase_id: RuStorePayPurchaseId, error: RuStoreError):
	_loading.visible = false
	OS.alert(error.description, error.name)
