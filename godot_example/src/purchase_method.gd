extends Node

@onready var _title = $background/box/MarginContainer/VBoxContainer/Title

var _on_one_step: Callable
var _on_two_step: Callable

func show(title: String, one_step_callback: Callable, two_step_callback: Callable):
	_title.text = title
	_on_one_step = one_step_callback
	_on_two_step = two_step_callback
	self.visible = true

func _on_one_step_pressed():
	self.visible = false
	_on_one_step.call()

func _on_two_step_pressed():
	self.visible = false
	_on_two_step.call()

func _on_cancel_pressed():
	self.visible = false
