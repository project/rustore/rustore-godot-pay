class_name ProductPanel extends MarginContainer

@onready var _productId = $ProductPanel/MarginContainer/VBoxContainer/ProductId
@onready var _productTitle = $ProductPanel/MarginContainer/VBoxContainer/ProductTitle
@onready var _productType = $ProductPanel/MarginContainer/VBoxContainer/ProductType
@onready var _productAmount = $ProductPanel/MarginContainer/VBoxContainer/ProductAmount
@onready var _productPrice = $ProductPanel/MarginContainer/VBoxContainer/ProductPrice

const ID_LABEL = "Id: "
const TITLE_LABEL = "Title: "
const TYPE_LABEL = "Type: "
const AMOUNT_LABEL = "Amount: "
const PRICE_LABEL = "Price: "

var _product: RuStorePayProduct = null

signal on_purchase_pressed

func set_product(product: RuStorePayProduct):
	_product = product
	_productId.text = ID_LABEL + _product.productId.value
	_productTitle.text = TITLE_LABEL + _product.title.value
	_productType.text = TYPE_LABEL + ERuStorePayProductType.Item.find_key(_product.type)
	_productAmount.text = AMOUNT_LABEL + _product.amountLabel.value
	_productPrice.text = PRICE_LABEL + str(_product.price.value)

func _on_purchase_pressed():
	on_purchase_pressed.emit(_product)
