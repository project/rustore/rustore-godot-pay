class_name PurchasePanel extends MarginContainer

@onready var _productId = $PurchasePanel/MarginContainer/VBoxContainer/ProductId
@onready var _purchaseId = $PurchasePanel/MarginContainer/VBoxContainer/PurchaseId
@onready var _purchaseTime = $PurchasePanel/MarginContainer/VBoxContainer/PurchaseTime
@onready var _purchaseType = $PurchasePanel/MarginContainer/VBoxContainer/PurchaseType
@onready var _purchaseAmount = $PurchasePanel/MarginContainer/VBoxContainer/PurchaseAmount
@onready var _purchaseOrderId = $PurchasePanel/MarginContainer/VBoxContainer/PurchaseOrderId
@onready var _purchaseStatus = $PurchasePanel/MarginContainer/VBoxContainer/PurchaseStatus

const PRODUCT_ID_LABEL = "ProductId: "
const PURCHASE_ID_LABEL = "PurchaseId: "
const TIME_LABEL = "Time: "
const TYPE_LABEL = "Type: "
const AMOUNT_LABEL = "Amount: "
const ORDER_ID_LABEL = "OrderId: "
const STATUS_LABEL = "Status: "

signal on_confirm_pressed
signal on_cancel_pressed
signal on_get_purchase_pressed

var _purchase: RuStorePayPurchase = null

func set_purchase(purchase: RuStorePayPurchase):
	_purchase = purchase
	_productId.text = PRODUCT_ID_LABEL + _purchase.productId.value
	_purchaseId.text = PURCHASE_ID_LABEL + _purchase.purchaseId.value
	if _purchase.purchaseTime != null:
		_purchaseTime.text = TIME_LABEL + _purchase.purchaseTime.value
	_purchaseType.text = TYPE_LABEL + ERuStorePayPurchaseType.Item.find_key(_purchase.purchaseType)
	_purchaseAmount.text = AMOUNT_LABEL + _purchase.amountLabel.value
	if _purchase.orderId != null:
		_purchaseOrderId.text = ORDER_ID_LABEL + _purchase.orderId.value
	_purchaseStatus.text = STATUS_LABEL + ERuStorePayPurchaseStatus.Item.find_key(_purchase.status)

func _on_confirm_pressed():
	on_confirm_pressed.emit(_purchase)

func _on_cancel_pressed():
	on_cancel_pressed.emit(_purchase)

func _on_get_purchase_pressed():
	on_get_purchase_pressed.emit(_purchase)
