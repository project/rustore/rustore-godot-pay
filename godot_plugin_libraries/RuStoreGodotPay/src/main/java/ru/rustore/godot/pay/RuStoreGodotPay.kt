package ru.rustore.godot.pay

import android.net.Uri
import android.util.ArraySet
import com.google.gson.GsonBuilder
import org.godotengine.godot.Dictionary
import org.godotengine.godot.Godot
import org.godotengine.godot.plugin.GodotPlugin
import org.godotengine.godot.plugin.SignalInfo
import org.godotengine.godot.plugin.UsedByGodot
import ru.rustore.godot.core.JsonBuilder
import ru.rustore.sdk.core.util.RuStoreUtils
import ru.rustore.sdk.pay.RuStorePayClient
import ru.rustore.sdk.pay.model.DeveloperPayload
import ru.rustore.sdk.pay.model.OrderId
import ru.rustore.sdk.pay.model.ProductId
import ru.rustore.sdk.pay.model.ProductPurchaseParams
import ru.rustore.sdk.pay.model.PurchaseAvailabilityResult
import ru.rustore.sdk.pay.model.PurchaseId
import ru.rustore.sdk.pay.model.Quantity

class RuStoreGodotPay(godot: Godot?) : GodotPlugin(godot) {
    private companion object {
        const val CHANNEL_GET_PURCHASE_AVAILABILITY_SUCCESS = "rustore_get_purchase_availability_success"
        const val CHANNEL_GET_PURCHASE_AVAILABILITY_FAILURE = "rustore_get_purchase_availability_failure"
        const val CHANNEL_ON_GET_PRODUCTS_SUCCESS = "rustore_on_get_products_success"
        const val CHANNEL_ON_GET_PRODUCTS_FAILURE = "rustore_on_get_products_failure"
        const val CHANNEL_ON_GET_PURCHASES_SUCCESS = "rustore_on_get_purchases_success"
        const val CHANNEL_ON_GET_PURCHASES_FAILURE = "rustore_on_get_purchases_failure"
        const val CHANNEL_ON_GET_PURCHASE_SUCCESS = "rustore_on_get_purchase_success"
        const val CHANNEL_ON_GET_PURCHASE_FAILURE = "rustore_on_get_purchase_failure"
        const val CHANNEL_ON_PURCHASE_ONE_STEP_SUCCESS = "rustore_on_purchase_one_step_success"
        const val CHANNEL_ON_PURCHASE_ONE_STEP_FAILURE = "rustore_on_purchase_one_step_failure"
        const val CHANNEL_ON_PURCHASE_TWO_STEP_SUCCESS = "rustore_on_purchase_two_step_success"
        const val CHANNEL_ON_PURCHASE_TWO_STEP_FAILURE = "rustore_on_purchase_two_step_failure"
        const val CHANNEL_ON_CONFIRM_TWO_STEP_PURCHASE_SUCCESS = "rustore_on_confirm_two_step_purchase_success"
        const val CHANNEL_ON_CONFIRM_TWO_STEP_PURCHASE_FAILURE = "rustore_on_confirm_two_step_purchase_failure"
        const val CHANNEL_ON_CANCEL_TWO_STEP_PURCHASE_SUCCESS = "rustore_on_cancel_two_step_purchase_success"
        const val CHANNEL_ON_CANCEL_TWO_STEP_PURCHASE_FAILURE = "rustore_on_cancel_two_step_purchase_failure"
    }

    override fun getPluginName(): String {
        return javaClass.simpleName
    }

    override fun getPluginSignals(): Set<SignalInfo> {
        val signals: MutableSet<SignalInfo> = ArraySet()
        signals.add(SignalInfo(CHANNEL_GET_PURCHASE_AVAILABILITY_SUCCESS, String::class.java))
        signals.add(SignalInfo(CHANNEL_GET_PURCHASE_AVAILABILITY_FAILURE, String::class.java))
        signals.add(SignalInfo(CHANNEL_ON_GET_PRODUCTS_SUCCESS, String::class.java))
        signals.add(SignalInfo(CHANNEL_ON_GET_PRODUCTS_FAILURE, String::class.java))
        signals.add(SignalInfo(CHANNEL_ON_GET_PURCHASES_SUCCESS, String::class.java))
        signals.add(SignalInfo(CHANNEL_ON_GET_PURCHASES_FAILURE, String::class.java))
        signals.add(SignalInfo(CHANNEL_ON_GET_PURCHASE_SUCCESS, String::class.java))
        signals.add(SignalInfo(CHANNEL_ON_GET_PURCHASE_FAILURE, String::class.java, String::class.java))
        signals.add(SignalInfo(CHANNEL_ON_PURCHASE_ONE_STEP_SUCCESS, String::class.java))
        signals.add(SignalInfo(CHANNEL_ON_PURCHASE_ONE_STEP_FAILURE, String::class.java, String::class.java))
        signals.add(SignalInfo(CHANNEL_ON_PURCHASE_TWO_STEP_SUCCESS, String::class.java))
        signals.add(SignalInfo(CHANNEL_ON_PURCHASE_TWO_STEP_FAILURE, String::class.java, String::class.java))
        signals.add(SignalInfo(CHANNEL_ON_CONFIRM_TWO_STEP_PURCHASE_SUCCESS, String::class.java))
        signals.add(SignalInfo(CHANNEL_ON_CONFIRM_TWO_STEP_PURCHASE_FAILURE, String::class.java, String::class.java))
        signals.add(SignalInfo(CHANNEL_ON_CANCEL_TWO_STEP_PURCHASE_SUCCESS, String::class.java))
        signals.add(SignalInfo(CHANNEL_ON_CANCEL_TWO_STEP_PURCHASE_FAILURE, String::class.java, String::class.java))

        return signals
    }

    private val gson = GsonBuilder()
        .registerTypeAdapter(Uri::class.java, UriTypeAdapter())
        .create()
    private var tag: String = ""
    private var allowErrorHandling: Boolean = false

    @UsedByGodot
    fun getPurchaseAvailability() {
        RuStorePayClient.instance.getPurchaseInteractor().getPurchaseAvailability()
            .addOnSuccessListener { result ->
                when (result) {
                    is PurchaseAvailabilityResult.Available -> {
                        emitSignal(CHANNEL_GET_PURCHASE_AVAILABILITY_SUCCESS, """{"isAvailable": true}""")
                    }
                    is PurchaseAvailabilityResult.Unavailable -> {
                        val cause = JsonBuilder.toJson(result.cause)
                        val json = """{"isAvailable": false, "cause": $cause}"""
                        emitSignal(CHANNEL_GET_PURCHASE_AVAILABILITY_SUCCESS, json)
                    }
                }
            }
            .addOnFailureListener { throwable ->
                emitSignal(CHANNEL_GET_PURCHASE_AVAILABILITY_FAILURE, JsonBuilder.toJson(throwable))
            }
    }

    @UsedByGodot
    fun isRuStoreInstalled(): Boolean =
        godot.getActivity()?.application?.let {
            return RuStoreUtils.isRuStoreInstalled(it)
        } ?: false

    @UsedByGodot
    fun getProducts(productIds: Array<String>) {
        val ids = productIds.map { ProductId(it) }.toList()
        RuStorePayClient.instance.getProductInteractor().getProducts(ids)
            .addOnSuccessListener { result ->
                emitSignal(CHANNEL_ON_GET_PRODUCTS_SUCCESS, gson.toJson(result))
            }
            .addOnFailureListener { throwable ->
                emitSignal(CHANNEL_ON_GET_PRODUCTS_FAILURE, JsonBuilder.toJson(throwable))
            }
    }

    @UsedByGodot
    fun getPurchases() {
        RuStorePayClient.instance.getPurchaseInteractor().getPurchases()
            .addOnSuccessListener { result ->
                emitSignal(CHANNEL_ON_GET_PURCHASES_SUCCESS, gson.toJson(result))
            }
            .addOnFailureListener { throwable ->
                emitSignal(CHANNEL_ON_GET_PURCHASES_FAILURE, JsonBuilder.toJson(throwable))
            }
    }

    @UsedByGodot
    fun getPurchase(purchaseId: String) {
        RuStorePayClient.instance.getPurchaseInteractor().getPurchase(PurchaseId(purchaseId))
            .addOnSuccessListener { result ->
                emitSignal(CHANNEL_ON_GET_PURCHASE_SUCCESS, gson.toJson(result))
            }
            .addOnFailureListener { throwable ->
                emitSignal(CHANNEL_ON_GET_PURCHASE_FAILURE, purchaseId, JsonBuilder.toJson(throwable))
            }
    }

    @UsedByGodot
    fun purchaseOneStep(params: Dictionary) {
        val developerPayload = params["developerPayload"]?.toString()
        val orderId = params["orderId"]?.toString()
        val productId = params["productId"].toString()
        val quantity = params["quantity"] as? Int

        RuStorePayClient.instance.getPurchaseInteractor().purchaseOneStep(
            ProductPurchaseParams(
                developerPayload = developerPayload?.let { DeveloperPayload(it) },
                orderId = orderId?.let{ OrderId(it) },
                productId = ProductId(productId),
                quantity = quantity?.let { Quantity(it) }
            ))
            .addOnSuccessListener { result ->
                val json = """{"type":"${result.javaClass.simpleName}","data":${gson.toJson(result)}}"""
                emitSignal(CHANNEL_ON_PURCHASE_ONE_STEP_SUCCESS, json)
            }
            .addOnFailureListener { throwable ->
                emitSignal(CHANNEL_ON_PURCHASE_ONE_STEP_FAILURE, productId, JsonBuilder.toJson(throwable))
            }
    }

    @UsedByGodot
    fun purchaseTwoStep(params: Dictionary) {
        val developerPayload = params["developerPayload"]?.toString()
        val orderId = params["orderId"]?.toString()
        val productId = params["productId"].toString()
        val quantity = params["quantity"] as? Int

        RuStorePayClient.instance.getPurchaseInteractor().purchaseTwoStep(
            ProductPurchaseParams(
                developerPayload = developerPayload?.let { DeveloperPayload(it) },
                orderId = orderId?.let{ OrderId(it) },
                productId = ProductId(productId),
                quantity = quantity?.let { Quantity(it) }
            ))
            .addOnSuccessListener { result ->
                val json = """{"type":"${result.javaClass.simpleName}","data":${gson.toJson(result)}}"""
                emitSignal(CHANNEL_ON_PURCHASE_TWO_STEP_SUCCESS, json)
            }
            .addOnFailureListener { throwable ->
                emitSignal(CHANNEL_ON_PURCHASE_TWO_STEP_FAILURE, productId, JsonBuilder.toJson(throwable))
            }
    }

    @UsedByGodot
    fun confirmTwoStepPurchase(purchaseId: String, developerPayload: String?) {
        RuStorePayClient.instance.getPurchaseInteractor()
            .confirmTwoStepPurchase(
                purchaseId = PurchaseId(purchaseId),
                developerPayload = developerPayload?.let { DeveloperPayload(it) })
            .addOnSuccessListener {
                emitSignal(CHANNEL_ON_CONFIRM_TWO_STEP_PURCHASE_SUCCESS, purchaseId)
            }
            .addOnFailureListener { throwable ->
                emitSignal(CHANNEL_ON_CONFIRM_TWO_STEP_PURCHASE_FAILURE, purchaseId, JsonBuilder.toJson(throwable))
            }
    }

    @UsedByGodot
    fun cancelTwoStepPurchase(purchaseId: String) {
        RuStorePayClient.instance.getPurchaseInteractor()
            .cancelTwoStepPurchase(PurchaseId(purchaseId))
            .addOnSuccessListener {
                emitSignal(CHANNEL_ON_CANCEL_TWO_STEP_PURCHASE_SUCCESS, purchaseId)
            }
            .addOnFailureListener { throwable ->
                emitSignal(CHANNEL_ON_CANCEL_TWO_STEP_PURCHASE_FAILURE, purchaseId, JsonBuilder.toJson(throwable))
            }
    }
}
